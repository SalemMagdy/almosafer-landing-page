$(document).ready(function(){
    var swiper = new Swiper('.hotels-slider', {
        slidesPerView: 3,
        spaceBetween: 30,
        slideVisibleClass:'.swiper-slide-visible',
        slidesPerView: 5,
        spaceBetween: 0,
        autoplay: {
            delay: 5000,
        },
        speed: 1600,
        navigation: {
            nextEl: '.swipe-right',
            prevEl: '.swipe-left',
        },
      });
      updateSlides();
      swiper.on('slideChange', function () {
        updateSlides();
      });
      function updateSlides() {
        $('.swiper-slide').removeClass('swiper-slide-visible swiper-slide-visible-1 swiper-slide-visible-2 swiper-slide-visible-3 swiper-slide-visible-4 swiper-slide-visible-5');
        $(swiper.slides[swiper.activeIndex]).addClass('swiper-slide-visible swiper-slide-visible-1');
        $(swiper.slides[swiper.activeIndex + 1]).addClass('swiper-slide-visible swiper-slide-visible-2');
        $(swiper.slides[swiper.activeIndex + 2]).addClass('swiper-slide-visible swiper-slide-visible-3');
        $(swiper.slides[swiper.activeIndex + 3]).addClass('swiper-slide-visible swiper-slide-visible-4');
        $(swiper.slides[swiper.activeIndex + 4]).addClass('swiper-slide-visible swiper-slide-visible-5');
      }
});